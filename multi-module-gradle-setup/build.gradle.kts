import com.runemate.game.api.bot.data.FeatureType
import com.runemate.gradle.RuneMatePlugin

plugins {
    id("java")
    kotlin("jvm") version "1.9.23"
    id("com.runemate") version "1.3.0"
}

repositories {
    mavenCentral()
}

subprojects {
    apply<JavaPlugin>()
    apply<RuneMatePlugin>()

    tasks.runClient {
        enabled = false
    }
}

runemate {
    devMode = true
    autoLogin = true

    manifests {
        create("Test Bot (Main)") {
            skip = true
            mainClass = "com.runemate.test.TestBot"
            tagline = "a test bot :)"
            description = "Tests stuff?"
            version = "1.0"
            internalId = "test-bot"

            obfuscation {
                +"obfuscation rule"
                exclude("obfuscation rule")
                exclude { "obfuscation rule" }
            }
            features {
                required(FeatureType.DIRECT_INPUT)
            }
            resources {
                +"resource rule"
                include("resource rule")
                include { "resource rule" }
            }
            variants {
                variant(name = "My Bot", price = BigDecimal.ZERO)
            }
        }
    }
}