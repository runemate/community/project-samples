import com.runemate.game.api.bot.data.FeatureType
import java.time.Duration

runemate {
    manifests {
        create("Test Bot #3") {
            skip = true
            mainClass = "com.runemate.test3.TestBot3"
            tagline = "a test bot :)"
            description = "Tests stuff?"
            version = "1.0"
            internalId = "test-bot3"

            obfuscation {
                +"obfuscation rule"
                exclude("obfuscation rule")
                exclude { "obfuscation rule" }
            }
            features {
                required(FeatureType.DIRECT_INPUT)
            }
            resources {
                +"resource rule"
                include("resource rule")
                include { "resource rule" }
            }
            price(0.20)
            trial {
                window = Duration.ofDays(7)
                allowance = Duration.ofHours(3)
            }
        }
    }
}