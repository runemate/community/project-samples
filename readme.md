### Sample Projects

This repository contains various sample projects to demonstrate how your project can be structured. We recommend that you use Gradle.

1. [Single-project Gradle Setup](/basic-gradle-setup) (recommended) - uses Gradle with the [RuneMate Gradle plugin](https://gitlab.com/runemate/community/runemate-gradle-plugin).
2. [Multi-project Gradle Setup](/multi-module-gradle-setup) - uses Gradle with the [RuneMate Gradle plugin](https://gitlab.com/runemate/community/runemate-gradle-plugin).
3. [Sample Projects](sample-bots) - sample "real" bot projects to help you get started.