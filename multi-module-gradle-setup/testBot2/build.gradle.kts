import com.runemate.game.api.bot.data.FeatureType
import java.time.Duration

runemate {
    manifests {
        create("Test Bot #2") {
            mainClass = "com.runemate.test2.TestBot2"
            tagline = "a test bot :)"
            description = "Tests stuff?"
            version = "1.0"
            internalId = "test-bot2"

            obfuscation {
                +"obfuscation rule"
                exclude("obfuscation rule")
                exclude { "obfuscation rule" }
            }
            features {
                required(FeatureType.DIRECT_INPUT)
            }
            resources {
                +"resource rule"
                include("resource rule")
                include { "resource rule" }
            }
            price(0.20)
            trial {
                window = Duration.ofDays(7)
                allowance = Duration.ofHours(3)
            }
        }
    }
}