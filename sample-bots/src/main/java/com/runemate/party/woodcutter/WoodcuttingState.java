package com.runemate.party.woodcutter;

public enum WoodcuttingState {
    CHOP,
    DROP
}
