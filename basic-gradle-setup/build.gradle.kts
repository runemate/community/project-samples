import com.runemate.game.api.bot.data.Category
import java.time.Duration

plugins {
    java
    id("com.runemate") version "1.4.1"
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

runemate {
    devMode = true
    autoLogin = true

    manifests {
        create("Basic Gradle Bot") {
            //This is the fully qualified name of your main class
            mainClass = "com.runemate.sample.basicgradle.BasicGradleProjectBot"

            //A short description that is shown under the bot name on the bot store
            tagline = "a test bot :)"

            //Shown in the bot description in the client
            description = "Tests stuff?"

            //The unique internal ID of the bot
            internalId = "test-bot"

            //The version of the bot
            version = "1.0.0"

            //The store supports multiple categories, the first will be the "main" category
            categories(Category.OTHER)

            //This is where you declare the price(s) of the bot
            variants {
                variant(name = "Variant name", price = 0.10)
            }

            //For premium bots, you can declare a "trial" which is a period for which a user can use the bot for free
            trial {
                window = Duration.ofDays(7)
                allowance = Duration.ofHours(3)
            }

            //Declare any resources used by the bot, relative to src/main/resources
            resources {
                include("fxml/example.fxml")
            }
        }
    }
}