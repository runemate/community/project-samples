package com.runemate.sample.basicgradle;

import com.runemate.game.api.script.framework.*;
import org.apache.logging.log4j.*;

public class BasicGradleProjectBot extends LoopingBot {

    //Declare a logger for this class
    //Alternatively, you can add a '@Log4j2' annotation to your class
    private static final Logger log = LogManager.getLogger(BasicGradleProjectBot.class);

    //This is where your bot logic should go
    @Override
    public void onLoop() {
        log.info("This is a sample bot!");
    }
}
