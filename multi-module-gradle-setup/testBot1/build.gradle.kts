import com.runemate.game.api.bot.data.FeatureType
import java.time.Duration

runemate {
    //In this example, we are excluding testBot1 from being submitted to the store
    //This is useful when you are working on a bot that you are not ready to publish yet
    excludeFromSubmission = true
    manifests {
        create("Test Bot #1") {
            mainClass = "com.runemate.test1.TestBot1"
            tagline = "a test bot :)"
            description = "Tests stuff?"
            version = "1.0"
            internalId = "test-bot1"

            obfuscation {
                +"obfuscation rule"
                exclude("obfuscation rule")
                exclude { "obfuscation rule" }
            }
            features {
                required(FeatureType.DIRECT_INPUT)
            }
            resources {
                +"resource rule"
                include("resource rule")
                include { "resource rule" }
            }
            price(0.20)
            trial {
                window = Duration.ofDays(7)
                allowance = Duration.ofHours(3)
            }
        }
    }
}